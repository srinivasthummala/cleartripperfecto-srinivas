package com.cleartrip.pages;

import com.google.common.base.CharMatcher;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class HotelsSearchTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "txt.set.destination.hotels")
	private QAFWebElement txtSetDestinationHotels;
	@FindBy(locator = "loc.common.resourceid.hotelSearchPage")
	private QAFWebElement locCommonResourceid;
	@FindBy(locator = "xpath.common.hotelSearchPage")
	private QAFWebElement locCommonxpath;
	@FindBy(locator = "lnk.hotel.destination.hotelSearchPage")
	private QAFWebElement lnkHotelDestination;
	@FindBy(locator = "txt.hotel.set.destination.hotelSearchPage")
	private QAFWebElement textHotelDestination;
	@FindBy(locator = "btn.hotel.search.hotelSearchPage")
	private QAFWebElement btnSearchHotels;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getTxtSetDestinationHotels() {
		return txtSetDestinationHotels;
	}

	public QAFWebElement getLocCommonResourceid() {
		return locCommonResourceid;
	}

	public QAFWebElement getHotelCity() {
		return lnkHotelDestination;
	}
	
	public QAFWebElement getHotelCityFilterEdit() {
		return textHotelDestination;
	}
	
	public QAFWebElement getBtnSearchHotel() {
		return btnSearchHotels;
	}
	
	/*public QAFWebElement getHotelCity() {
		QAFWebElement element =
				new QAFExtendedWebElement(String.format(
						ConfigurationManager.getBundle()
								.getString("loc.common.resourceid.hotelSearchPage"),
						"txt_hotel_city"));
		return element;
	}*/

	/*public QAFWebElement getHotelCityFilterEdit() {
		QAFWebElement element =
				new QAFExtendedWebElement(String.format(
						ConfigurationManager.getBundle()
								.getString("loc.common.resourceid.hotelSearchPage"),
						"filter_edittext"));
		return element;
	}*/

	public QAFWebElement getSelectHotelCity(String location) {
		QAFWebElement element =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("lnk.hotel.select.destination.hotelSearchPage"), location+","));
		return element;
	}

	/*public QAFWebElement getBtnSearchHotel() {
		QAFWebElement element =
				new QAFExtendedWebElement(String.format(
						ConfigurationManager.getBundle()
								.getString("loc.common.resourceid.hotelSearchPage"),
						"btn_search_hotels"));
		return element;
	}*/

	public void waitForPageToLoad() {
		getBtnSearchHotel().isPresent();
	}

	public void selectCity(String location) {
		getHotelCity().isPresent();
		Reporter.log("selecting hotel city element to pass city name");
		getHotelCity().click();
		getHotelCityFilterEdit().click();
		Reporter.log("filtering city element to select location");
		getHotelCityFilterEdit().sendKeys(location);
		//getSelectHotelCity(location).isPresent();
		Reporter.log("selecting city to search for hotels");
		getSelectHotelCity(location).click();

	}

	public void btnClickSearchHotel() {
		getBtnSearchHotel().isDisplayed();
		Reporter.log("clicking search hotels button");
		getBtnSearchHotel().click();
	}

	public Integer extractNumericFromString(String str) {
		String theDigits = CharMatcher.DIGIT.retainFrom(str);
		return Integer.parseInt(theDigits);
	}

	

}
