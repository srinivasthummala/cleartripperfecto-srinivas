package com.cleartrip.pages;

import java.util.HashMap;
import java.util.Map;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HotelDetailsTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "common.text.productDetailsPage")
	private QAFWebElement commonTextProductDetailsPage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getCommonTextProductDetailsPage() {
		return commonTextProductDetailsPage;
	}

	public QAFWebElement getHotelNameLabel(String text) {
		QAFWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("common.text.productDetailsPage"), text));
		return element;
	}
	
	public boolean isHotelNameLablePresent(String name) {
		return getHotelNameLabel(name).isPresent();
	}
	
	public void closeApp() {
		Map<String, Object> params = new HashMap<>();
		   
		params.put("identifier", "com.cleartrip.android");
		driver.executeScript("mobile:application:close", params);
	}
	
}
