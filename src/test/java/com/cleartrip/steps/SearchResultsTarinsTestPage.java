package com.cleartrip.steps;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

import java.util.List;

import org.hamcrest.Matchers;

import com.cleartrip.bean.CleartripBean;
import com.cleartrip.component.ComponentCleartripPage;

public class SearchResultsTarinsTestPage
		extends
			WebDriverBaseTestPage<WebDriverTestPage> {

	protected void openPage(PageLocator locator, Object... args) {

	}

	@FindBy(locator = "component.trainssearchresultpage.xpath")
	private List<ComponentCleartripPage> componentTrainssearchresultpageXpath;

	public List<ComponentCleartripPage> getComponentTrainssearchresultpageXpath() {
		return componentTrainssearchresultpageXpath;
	}

	@QAFTestStep(description = "user on search results page")
	public void searchResults() {
		Validator.verifyThat(getComponentTrainssearchresultpageXpath().size(),
				Matchers.greaterThan(0));
		int comsize = getComponentTrainssearchresultpageXpath().size();
		System.out.println("Size:"+comsize);

		for (ComponentCleartripPage component : getComponentTrainssearchresultpageXpath()) {

			Reporter.log("Train Number:"
					+ component.getTrainnumberSearchresultspageXpath().getText());
			System.out.println("TrainNumber:"
					+ component.getTrainnumberSearchresultspageXpath().getText());
		}
	}
	
	
	public void beanVerify(int index) {

		CleartripBean trainsBean = new CleartripBean();

		trainsBean.setTrainNumber(getComponentTrainssearchresultpageXpath().get(index)
				.getTrainnumberSearchresultspageXpath().getText());

	}
}
