package com.cleartrip.steps;

import com.cleartrip.pages.HomeTestPage;
import com.cleartrip.pages.HotelsSearchTestPage;
import com.qmetry.qaf.automation.step.QAFTestStep;

public class HotelSearchPageSteps {

	HomeTestPage homePge = new HomeTestPage();
	HotelsSearchTestPage hotelSearchPage = new HotelsSearchTestPage();

	@QAFTestStep(description = "user set {0} location for finding hotels")
	public void userSetLocationForFindingHotels(String location) {
		hotelSearchPage.selectCity(location);
	}

	@QAFTestStep(description = "user clicks on search hotels button")
	public void userClicksSearchHotelBtn() {
		
		hotelSearchPage.btnClickSearchHotel();
	}

}
